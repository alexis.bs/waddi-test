import { Table, Column, Model, DataType, ForeignKey, BelongsTo, createIndexDecorator } from 'sequelize-typescript';
import { User } from '../../../user/infrastructure/persistence/user.schema';
import { Post } from 'src/post/infrastructure/persistence/post.schema';

const ReviewIndex = createIndexDecorator({
  type: 'UNIQUE',
});

@Table
export class Review extends Model<Review> {
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    validate: {
      min: 0,
      max: 5,
    },
  })
  value: number;

  @ReviewIndex
  @ForeignKey(() => Post)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  postId: number;

  @BelongsTo(() => Post)
  post: Post;

  @ReviewIndex
  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  userId: number;

  @BelongsTo(() => User)
  user: User;
}
