import { Review } from './persistence/review.schema';
import { REVIEW_REPOSITORY } from '../../shared/domain/const';
export const reviewsProviders = [
  {
    provide: REVIEW_REPOSITORY,
    useValue: Review,
  },
];
