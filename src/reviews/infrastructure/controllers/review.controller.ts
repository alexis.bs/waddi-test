import { Controller, Post, Put, Param, Body, Request } from '@nestjs/common';
import { ReviewService } from '../../application/review.service';
import { ReviewDto } from '../../application/dtos/review-data.dto';
import { Review as ReviewEntity } from '../persistence/review.schema';
import { Auth } from 'src/auth/domain/decorator/auth.decorator';

@Controller('reviews')
export class ReviewController {
  constructor(private reviewService: ReviewService) {}

  @Auth()
  @Post()
  async create(@Body() review: ReviewDto, @Request() req: any): Promise<ReviewEntity> {
    return await this.reviewService.create(review, req.user.id);
  }

  @Auth()
  @Put(':id')
  async update(@Param('id') id: number, @Body() review: ReviewDto, @Request() req: any): Promise<ReviewEntity> {
    const updatedPost = await this.reviewService.update(id, review, req.user.id);

    return updatedPost;
  }
}
