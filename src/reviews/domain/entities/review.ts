export interface ReviewInterface {
  userId: number;
  postId: number;
  value: number;
}
