import { IsNotEmpty } from 'class-validator';

export class ReviewDto {
  @IsNotEmpty()
  readonly postId: number;

  @IsNotEmpty()
  readonly value: number;
}
