import { Injectable, Logger, Inject, NotFoundException } from '@nestjs/common';
import { Review } from '../infrastructure/persistence/review.schema';
import { ReviewDto } from './dtos/review-data.dto';
import { REVIEW_REPOSITORY } from '../../shared/domain/const';

@Injectable()
export class ReviewService {
  private readonly logger = new Logger(ReviewService.name);
  constructor(
    @Inject(REVIEW_REPOSITORY)
    private readonly reviewRepository: typeof Review
  ) {}

  async create(review: ReviewDto, userId: number): Promise<Review> {
    return await this.reviewRepository.create<Review>({ ...review, userId });
  }
  async update(id: number, data: ReviewDto, userId: any): Promise<any> {
    const exist = await this.reviewRepository.findOne({
      where: { id, userId },
    });
    if (!exist) {
      this.logger.log("This Post doesn't exist");
      throw new NotFoundException("This Post doesn't exist");
    }
    await this.reviewRepository.update({ ...data }, { where: { id, userId }, returning: true });

    return data;
  }

  async deleteForUser(userId: number): Promise<any> {
    try {
      return await this.reviewRepository.destroy({ where: { userId } });
    } catch (error) {
      this.logger.error('Error to delete reviews of user');
    }
  }
}
