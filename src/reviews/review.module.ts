import { Module } from '@nestjs/common';
import { ReviewController } from './infrastructure/controllers/review.controller';
import { ReviewService } from './application/review.service';
import { reviewsProviders } from './infrastructure/review.provider';
import { UserService } from 'src/user/application/user.service';
import { usersProviders } from 'src/user/infrastructure/user.provider';
import { HistoryService } from 'src/history/application/history.service';
import { historyProviders } from 'src/history/infrastructure/history.provider';
import { PostService } from 'src/post/application/post.service';
import { postsProviders } from 'src/post/infrastructure/post.provider';
@Module({
  controllers: [ReviewController],
  providers: [
    ReviewService,
    ...reviewsProviders,
    UserService,
    ...usersProviders,
    HistoryService,
    ...historyProviders,
    PostService,
    ...postsProviders,
  ],
  exports: [ReviewService],
})
export class ReviewModule {}
