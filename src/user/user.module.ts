import { Module } from '@nestjs/common';
import { UserController } from './infrastructure/controllers/user.controller';
import { UserService } from './application/user.service';
import { usersProviders } from './infrastructure/user.provider';
import { HistoryService } from 'src/history/application/history.service';
import { historyProviders } from 'src/history/infrastructure/history.provider';
import { PostService } from 'src/post/application/post.service';
import { postsProviders } from 'src/post/infrastructure/post.provider';
import { ReviewService } from 'src/reviews/application/review.service';
import { reviewsProviders } from 'src/reviews/infrastructure/review.provider';
@Module({
  controllers: [UserController],
  providers: [
    UserService,
    ...usersProviders,
    HistoryService,
    ...historyProviders,
    PostService,
    ...postsProviders,
    ReviewService,
    ...reviewsProviders,
  ],
  exports: [UserService, ...usersProviders],
})
export class UserModule {}
