import { Injectable, Logger, Inject, NotFoundException } from '@nestjs/common';
import { UserInterface } from '../domain/entities/user';
import { User } from '../infrastructure/persistence/user.schema';
import { UserDto } from './dtos/user-data.dto';
import { USER_REPOSITORY } from '../../shared/domain/const';
import { PostService } from 'src/post/application/post.service';
import { HistoryService } from 'src/history/application/history.service';
import { ReviewService } from 'src/reviews/application/review.service';

import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  private readonly logger = new Logger(UserService.name);
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly userRepository: typeof User,
    private readonly historyService: HistoryService,
    private readonly postService: PostService,
    private readonly reviewService: ReviewService
  ) {}

  async createUser(user: UserDto): Promise<UserInterface> {
    this.logger.log(`Success`);

    return await this.userRepository.create<User>(user);
  }

  async findOneByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne<User>({ where: { email } });
  }

  async findOneById(id: number): Promise<User> {
    return await this.userRepository.findOne<User>({ where: { id } });
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.findAll<User>({
      include: [{ model: User, attributes: { exclude: ['password'] } }],
    });
  }
  async findUserById(id: number): Promise<User> {
    return await this.userRepository.findOne<User>({ where: { id }, attributes: { exclude: ['password'] } });
  }

  async delete(id: number): Promise<any> {
    await this.postService.deleteForUser(id);
    await this.historyService.delete(id);
    await this.reviewService.deleteForUser(id);
    return await this.userRepository.destroy({ where: { id } });
  }

  async update(id: number, data: any): Promise<any> {
    const roles = data.roles.split(',');
    const permissions = {
      admin: 'admin',
      user: 'user',
      userManager: 'userManager',
      userEdit: 'userEdit',
    };

    roles.forEach((element) => {
      if (!permissions[element]) {
        throw new NotFoundException(
          "the role its format incorret. the correct format its : 'user,userEdit,userManager' or 'user,UserManager', etc "
        );
      }
    });
    if (data.password) {
      data.password = await bcrypt.hash(data.password, 10);
    }
    await this.userRepository.update({ ...data }, { where: { id }, returning: true });

    return await this.userRepository.findOne<User>({ where: { id }, attributes: { exclude: ['password'] } });
  }
}
