import { IsEmail, IsOptional } from 'class-validator';

export class UserUpdateDataDto {
  @IsOptional()
  readonly name: string;

  @IsOptional()
  @IsEmail()
  readonly email: string;

  @IsOptional()
  readonly roles: string;

  @IsOptional()
  readonly password: string;
}
