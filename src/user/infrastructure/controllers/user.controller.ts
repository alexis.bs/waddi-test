import { Body, Controller, Get, NotFoundException, Param, Put, Delete } from '@nestjs/common';

import { UserService } from '../../application/user.service';
import { User as UserEntity } from '../persistence/user.schema';
import { Admin } from 'src/auth/domain/decorator/admin.decorator';
import { UserUpdateDataDto } from 'src/user/application/dtos/user-update-data.dto';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Admin()
  @Get()
  async findAll(): Promise<UserEntity[]> {
    return await this.userService.findAll();
  }

  @Admin()
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<UserEntity> {
    const post = await this.userService.findUserById(id);

    if (!post) {
      throw new NotFoundException("This user doesn't exist");
    }

    return post;
  }

  @Admin()
  @Put(':id')
  async update(@Param('id') id: number, @Body() user: UserUpdateDataDto): Promise<UserEntity> {
    const updatedPost = await this.userService.update(id, user);

    return updatedPost;
  }

  @Admin()
  @Delete(':id')
  async remove(@Param('id') id: number): Promise<string> {
    const deleted = await this.userService.delete(id);

    if (deleted === 0) {
      throw new NotFoundException("This user doesn't exist");
    }

    return 'Successfully deleted';
  }
}
