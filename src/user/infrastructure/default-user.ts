import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { User } from 'src/user/infrastructure/persistence/user.schema';
import { EnvVariablesService } from '../../shared/Domain/Config/env-variables.service';
import { USER_REPOSITORY } from 'src/shared/domain/const';
import * as bcrypt from 'bcrypt';

@Injectable()
export class LoadSeeds implements OnApplicationBootstrap {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly userRepository: typeof User,
    private readonly envVariablesService: EnvVariablesService
  ) {}
  async onApplicationBootstrap(): Promise<any> {
    const hash = await bcrypt.hash(this.envVariablesService.getUserPasswordDefault, 10);
    const newUser = {
      name: 'Test',
      email: this.envVariablesService.getUserDefault,
      password: hash,
      roles: 'admin',
    };
    const email = this.envVariablesService.getUserDefault;
    const exist = await this.userRepository.findOne<User>({ where: { email } });
    if (!exist) {
      this.userRepository.create<User>(newUser);
    }
  }
}
