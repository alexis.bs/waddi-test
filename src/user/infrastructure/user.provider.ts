import { User } from './persistence/user.schema';
import { USER_REPOSITORY } from './../../shared/domain/const';
export const usersProviders = [
  {
    provide: USER_REPOSITORY,
    useValue: User,
  },
];
