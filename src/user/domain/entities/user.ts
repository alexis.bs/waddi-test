export interface UserInterface {
  name: string;
  email: string;
  password: string;
  roles: string;
}
