import { Module } from '@nestjs/common';
import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import { PostModule } from './post/post.module';
import { AuthModule } from './auth/auth.module';
import { ENVS } from './shared/domain/config/schema-env-variables';
import { LoggerModule } from 'nestjs-pino';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './shared/domain/infrastructure/database/database.module';
import { LoadSeeds } from './user/infrastructure/default-user';
import { ReviewModule } from './reviews/review.module';
import { HistoryModule } from './history/history.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    DatabaseModule,
    LoggerModule.forRoot({
      pinoHttp: {
        level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
        transport: process.env.NODE_ENV === ENVS.LOCAL ? { target: 'pino-pretty' } : undefined,
        serializers: {
          req(req) {
            req.body = req.raw.body;
            return req;
          },
        },
      },
    }),
    SharedModule,
    UserModule,
    PostModule,
    AuthModule,
    ReviewModule,
    HistoryModule,
  ],
  providers: [LoadSeeds],
})
export class AppModule {}
