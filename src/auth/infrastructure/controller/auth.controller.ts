import { Controller, Body, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './../../application/auth.service';
import { UserDto } from '../../../user/application/dtos/user-data.dto';
import { LocalAuthGuard } from 'src/auth/domain/guard/local-auth-guard';
import { User } from 'src/auth/domain/decorator/user-decorator';
import { Admin } from 'src/auth/domain/decorator/admin.decorator';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@User() user: any): Promise<any> {
    return await this.authService.login(user);
  }

  @Admin()
  @Post('signup')
  async signUp(@Body() user: UserDto): Promise<any> {
    return await this.authService.create(user);
  }
}
