import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { UserService } from '../../user/application/user.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);
  constructor(private readonly userService: UserService, private readonly jwtService: JwtService) {}
  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.userService.findOneByEmail(email);
    if (!user) {
      this.logger.error(`User no exist`);
      throw new BadRequestException('User no exist');
    }

    const match = await this.comparePassword(pass, user.password);

    if (!match) {
      this.logger.error(`password no match`);
      throw new BadRequestException('password no match');
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = user['dataValues'];
    return result;
  }

  public async login(user: any): Promise<any> {
    const token = await this.generateToken(user);
    return { user, token };
  }

  public async create(user: any): Promise<any> {
    const userExist = await this.userService.findOneByEmail(user.email);
    if (userExist) {
      this.logger.error(`User duplicate`);
      throw new BadRequestException('User duplicate');
    }

    const pass = await this.hashPassword(user.password);

    const newUser = await this.userService.createUser({ ...user, password: pass });

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...result } = newUser['dataValues'];

    const token = await this.generateToken(result);

    return { user: result, token };
  }

  private async generateToken(user) {
    const token = await this.jwtService.signAsync(user);
    return token;
  }

  private async hashPassword(password) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  private async comparePassword(enteredPassword, dbPassword) {
    const match = bcrypt.compare(enteredPassword, dbPassword);
    return match;
  }
}
