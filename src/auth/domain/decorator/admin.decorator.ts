import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AdminGuard } from '../guard/admin-guard';

export const Admin = (): any => {
  return UseGuards(AuthGuard('jwt'), AdminGuard);
};
