import { applyDecorators, UseGuards } from '@nestjs/common';
import { jwtAuthGuard } from './../../domain/guard';

export function Auth(): any {
  return applyDecorators(UseGuards(jwtAuthGuard));
}
