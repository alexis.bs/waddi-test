import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { EditGuard } from '../guard/user-edit-guard';

export const Edit = (): any => {
  return UseGuards(AuthGuard('jwt'), EditGuard);
};
