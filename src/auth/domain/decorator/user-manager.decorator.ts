import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserManagerGuard } from '../guard/user-manager-guard';

export const UserManager = (): any => {
  return UseGuards(AuthGuard('jwt'), UserManagerGuard);
};
