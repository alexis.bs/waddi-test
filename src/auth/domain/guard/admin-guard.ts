import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { Observable } from 'rxjs';

@Injectable()
export class AdminGuard implements CanActivate {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const permissions = {
      Admin: 'admin',
      User: 'user',
      UserManager: 'userManager',
      UserEdit: 'userEdit',
    };

    const user = request.user;
    return user.roles.includes(permissions.Admin);
  }
}
