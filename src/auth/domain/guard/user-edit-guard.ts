import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { UserService } from 'src/user/application/user.service';
import { Observable } from 'rxjs';

@Injectable()
export class EditGuard implements CanActivate {
  constructor(private readonly userService: UserService) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.getUser(request);
  }

  async getUser(request: any): Promise<any> {
    const permissions = {
      Admin: 'admin',
      User: 'user',
      UserManager: 'userManager',
      UserEdit: 'userEdit',
    };
    const user = await this.userService.findOneById(request.user.id);

    return user.dataValues.roles.includes(permissions.UserEdit);
  }
}
