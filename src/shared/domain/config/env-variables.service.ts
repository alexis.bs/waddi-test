import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { ENVS } from './schema-env-variables';

@Injectable()
export class EnvVariablesService {
  constructor(private configService: ConfigService) {}

  get IsDevEnv(): boolean {
    return [ENVS.LOCAL, ENVS.DEVELOPMENT].includes(this.configService.get('NODE_ENV'));
  }

  get IsLocal(): boolean {
    return this.configService.get('NODE_ENV') === ENVS.LOCAL;
  }

  get RunningEnv(): string {
    return this.IsDevEnv ? 'pre' : 'pro';
  }

  get AppPort(): number {
    return this.configService.get<number>('PORT');
  }

  get DatabasePort(): number {
    return this.configService.get<number>('PORTDATABASE');
  }
  get DatabaseDialect(): string {
    return this.configService.get<string>('DIALECT');
  }
  get DatabaseHost(): string {
    return this.configService.get<string>('HOST');
  }

  get DatabaseUsername(): string {
    return this.configService.get<string>('USERNAME');
  }
  get DatabasePassword(): string {
    return this.configService.get<string>('PASSWORD');
  }
  get DatabaseName(): string {
    return this.configService.get<string>('DATABASE');
  }
  get getJwt(): string {
    return this.configService.get<string>('JWTKEY');
  }
  get getUserDefault(): string {
    return this.configService.get<string>('DEFAULT_USER');
  }

  get getUserPasswordDefault(): string {
    return this.configService.get<string>('DEFAULT_USER_PASSWORD');
  }
}
