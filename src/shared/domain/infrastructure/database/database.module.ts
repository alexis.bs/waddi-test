import { Module } from '@nestjs/common';

import { databaseProviders } from './database.providers';
import { UserModule } from './../../../../user/user.module';

@Module({
  providers: [...databaseProviders, UserModule],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
