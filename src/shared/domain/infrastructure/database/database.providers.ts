import { Sequelize } from 'sequelize-typescript';

import { SEQUELIZE, DEVELOPMENT, TEST, PRODUCTION } from '../../const';
import { databaseConfig } from './../database/database.config';
import { User } from '../../../../user/infrastructure/persistence/user.schema';
import { Post } from '../../../../post/infrastructure/persistence/post.schema';
import { Review } from '../../../../reviews/infrastructure/persistence/review.schema';
import { History } from 'src/history/infrastructure/persistence/history.schema';

export const databaseProviders = [
  {
    provide: SEQUELIZE,
    useFactory: async (): Promise<any> => {
      let config;
      switch (process.env.NODE_ENV) {
        case DEVELOPMENT:
          config = databaseConfig.development;
          break;
        case TEST:
          config = databaseConfig.test;
          break;
        case PRODUCTION:
          config = databaseConfig.production;
          break;
        default:
          config = databaseConfig.development;
      }
      const sequelize = new Sequelize(config);
      sequelize.addModels([User, Post, Review, History]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
