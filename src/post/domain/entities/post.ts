export interface PostInterface {
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
}
