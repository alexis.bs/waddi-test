import { Module } from '@nestjs/common';
import { PostController } from './infrastructure/controllers/post.controller';
import { PostService } from './application/post.service';
import { postsProviders } from './infrastructure/post.provider';
import { UserService } from 'src/user/application/user.service';
import { usersProviders } from 'src/user/infrastructure/user.provider';
import { HistoryService } from 'src/history/application/history.service';
import { historyProviders } from 'src/history/infrastructure/history.provider';
import { ReviewService } from 'src/reviews/application/review.service';
import { reviewsProviders } from 'src/reviews/infrastructure/review.provider';

@Module({
  controllers: [PostController],
  providers: [
    PostService,
    ...postsProviders,
    UserService,
    ...usersProviders,
    HistoryService,
    ...historyProviders,
    ReviewService,
    ...reviewsProviders,
  ],
  exports: [PostService],
})
export class PostModule {}
