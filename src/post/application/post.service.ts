import { Injectable, Logger, Inject, NotFoundException } from '@nestjs/common';
import { Post } from '../infrastructure/persistence/post.schema';
import { PostDto } from './dtos/post-data.dto';
import { POST_REPOSITORY } from '../../shared/domain/const';
import { HistoryService } from '../../history/application/history.service';
import { Op } from 'sequelize';

@Injectable()
export class PostService {
  private readonly logger = new Logger(PostService.name);
  constructor(
    @Inject(POST_REPOSITORY)
    private readonly postRepository: typeof Post,
    private readonly historyService: HistoryService
  ) {}

  async create(post: PostDto, userId: number): Promise<Post> {
    let newPost;
    try {
      newPost = await this.postRepository.create<Post>({ ...post, userId });

      const action = `El usuario ${userId} ha creado el post: ${newPost.id}`;

      this.historyService.create({ action }, userId);
    } catch (error) {
      this.logger.error('Error to create new Post');
    }
    return newPost;
  }

  async findAll(fechaInicio: string, fechaFin: string): Promise<Post[]> {
    if (fechaInicio && fechaFin) {
      return await this.postRepository.findAll<Post>({
        where: {
          createdAt: {
            [Op.between]: [new Date(fechaInicio), new Date(fechaFin)],
          },
        },

        order: [['updatedAt', 'DESC']],
      });
    }
    if (fechaFin) {
      return await this.postRepository.findAll<Post>({
        where: {
          createdAt: {
            [Op.between]: [new Date(), new Date(fechaFin)],
          },
        },
        order: [['updatedAt', 'DESC']],
      });
    }
    if (fechaInicio) {
      return await this.postRepository.findAll<Post>({
        where: {
          createdAt: {
            [Op.gte]: new Date(fechaInicio),
          },
        },
        order: [['updatedAt', 'DESC']],
      });
    }

    return await this.postRepository.findAll<Post>({ order: [['updatedAt', 'DESC']] });
  }

  async findWhere(): Promise<Post[]> {
    return await this.postRepository.findAll<Post>({});
  }

  async findOne(id: number): Promise<Post> {
    return await this.postRepository.findOne({
      where: { id },
    });
  }

  async delete(id: number, userId: number): Promise<any> {
    let deleted;
    try {
      deleted = await this.postRepository.destroy({ where: { id, userId } });
      const action = `El usuario ${userId} ha eliminado el post: ${id}`;

      this.historyService.create({ action }, userId);
    } catch (error) {
      this.logger.error('Error to delete  Post');
    }
    return deleted;
  }

  async update(id: number, data: PostDto, userId: any): Promise<any> {
    const exist = await this.postRepository.findOne({
      where: { id },
    });
    if (!exist) {
      throw new NotFoundException("This Post doesn't exist");
    }
    await this.postRepository.update({ ...data }, { where: { id, userId }, returning: true });
    const action = `El usuario ${userId} ha actualizado el post: ${id}`;
    this.historyService.create({ action }, userId);

    return data;
  }

  async deleteForUser(userId: number): Promise<any> {
    try {
      return await this.postRepository.destroy({ where: { userId } });
    } catch (error) {
      this.logger.error('Error to delete  Posts of user');
    }
  }
}
