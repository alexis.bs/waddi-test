import { Controller, Get, Post, Put, Delete, Param, Body, NotFoundException, Request, Query } from '@nestjs/common';
import { PostService } from '../../application/post.service';
import { PostDto } from '../../application/dtos/post-data.dto';
import { Post as PostEntity } from '../../infrastructure/persistence/post.schema';
import { UserManager } from 'src/auth/domain/decorator/user-manager.decorator';
import { Edit } from 'src/auth/domain/decorator/user-edit.decorator';

@Controller('posts')
export class PostController {
  constructor(private postService: PostService) {}

  @Get()
  async findAll(
    @Query('fechaInicio') fechaInicio?: string,
    @Query('fechaFin') fechaFin?: string
  ): Promise<PostEntity[]> {
    return await this.postService.findAll(fechaInicio, fechaFin);
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<PostEntity> {
    const post = await this.postService.findOne(id);

    if (!post) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return post;
  }

  @UserManager()
  @Post()
  async create(@Body() post: PostDto, @Request() req: any): Promise<PostEntity> {
    return await this.postService.create(post, req.user.id);
  }

  @Edit()
  @Put(':id')
  async update(@Param('id') id: number, @Body() post: PostDto, @Request() req: any): Promise<PostEntity> {
    const updatedPost = await this.postService.update(id, post, req.user.id);

    return updatedPost;
  }

  @UserManager()
  @Delete(':id')
  async remove(@Param('id') id: number, @Request() req: any): Promise<string> {
    const deleted = await this.postService.delete(id, req.user.id);

    if (deleted === 0) {
      throw new NotFoundException("This Post doesn't exist");
    }

    return 'Successfully deleted';
  }
}
