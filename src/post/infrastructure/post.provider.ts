import { Post } from './persistence/post.schema';
import { POST_REPOSITORY } from '../../shared/domain/const';
export const postsProviders = [
  {
    provide: POST_REPOSITORY,
    useValue: Post,
  },
];
