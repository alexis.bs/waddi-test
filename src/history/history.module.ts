import { Module } from '@nestjs/common';
import { HistoryService } from './application/history.service';
import { historyProviders } from './infrastructure/history.provider';

@Module({
  controllers: [],
  providers: [HistoryService, ...historyProviders],
  exports: [HistoryService],
})
export class HistoryModule {}
