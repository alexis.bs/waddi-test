import { Injectable, Logger, Inject } from '@nestjs/common';
import { History } from '../infrastructure/persistence/history.schema';
import { HistoryDto } from './dtos/history-data.dto';
import { HISTORY_REPOSITORY } from '../../shared/domain/const';

@Injectable()
export class HistoryService {
  private readonly logger = new Logger(HistoryService.name);
  constructor(
    @Inject(HISTORY_REPOSITORY)
    private readonly historyRepository: typeof History
  ) {}

  async create(review: HistoryDto, userId: number): Promise<History> {
    return await this.historyRepository.create<History>({ ...review, userId });
  }
  async delete(userId: number): Promise<any> {
    return await this.historyRepository.destroy({ where: { userId } });
  }
}
