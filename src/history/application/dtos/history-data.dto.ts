import { IsNotEmpty } from 'class-validator';

export class HistoryDto {
  @IsNotEmpty()
  readonly action: string;
}
