export interface HistoryInterface {
  userId: number;
  action: string;
}
