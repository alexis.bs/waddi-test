import { History } from './persistence/history.schema';
import { HISTORY_REPOSITORY } from '../../shared/domain/const';
export const historyProviders = [
  {
    provide: HISTORY_REPOSITORY,
    useValue: History,
  },
];
