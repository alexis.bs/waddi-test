import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { User } from '../../../user/infrastructure/persistence/user.schema';

@Table
export class History extends Model<History> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  action: string;

  @ForeignKey(() => User)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  userId: number;

  @BelongsTo(() => User)
  user: User;
}
