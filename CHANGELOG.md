# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://gitlab.com/alexis.bs/waddi-test/-/compare/v0.2.0...v1.0.0) (2023-02-19)


### Features

* add const for project ([30ef349](https://gitlab.com/alexis.bs/waddi-test/-/commit/30ef3496fb89c8e87e9c800c4f7abb57718858fe))
* add controller , service and module of auth module ([0a65ba9](https://gitlab.com/alexis.bs/waddi-test/-/commit/0a65ba9f149a12d5a581ca01cf36e28a59e68a4c))
* add database config and provider ([3ff747a](https://gitlab.com/alexis.bs/waddi-test/-/commit/3ff747a140384bd2fffab0419c2aa3cff4717045))
* add decorator for auth module ([d5d57b4](https://gitlab.com/alexis.bs/waddi-test/-/commit/d5d57b4e679ad401b342a9a278f7415f0d1e0d43))
* add dto for request in service ([a02e8fa](https://gitlab.com/alexis.bs/waddi-test/-/commit/a02e8fa96eda7cd7aa208d4b9e5e8f9ac9b2c751))
* add en variables service ([71088ac](https://gitlab.com/alexis.bs/waddi-test/-/commit/71088acbafb6fdb7d4f87cf8f478c33233116d7c))
* add entity , dto and service for history module ([c102831](https://gitlab.com/alexis.bs/waddi-test/-/commit/c102831a33667b186c3b9801e81522daf89686a8))
* add entity and dto for post ([10dded4](https://gitlab.com/alexis.bs/waddi-test/-/commit/10dded488bb7759d26b8a7ace01a26a08360753c))
* add entity and dtos for user module ([34091e9](https://gitlab.com/alexis.bs/waddi-test/-/commit/34091e9e3a1e85042401f949c6a8490a8dc661b0))
* add guards for auth module ([c8ca2df](https://gitlab.com/alexis.bs/waddi-test/-/commit/c8ca2df96db759b8c9e5b2676dadb330dcf90280))
* add history provider and schema ([d464737](https://gitlab.com/alexis.bs/waddi-test/-/commit/d464737bf83fb486afb9b449870cbe59768afb56))
* add module history ([b8002ae](https://gitlab.com/alexis.bs/waddi-test/-/commit/b8002ae1c6d43cc6d6cc92a463122cb45e6812d7))
* add module of shared ([0599090](https://gitlab.com/alexis.bs/waddi-test/-/commit/0599090679149a7609d22263abe223f5495003a6))
* add provider and schema of post module ([392ad8b](https://gitlab.com/alexis.bs/waddi-test/-/commit/392ad8bff1ec16c46af6bdc58fb1668484660f9a))
* add schema, provider and seed for user module ([83a70cb](https://gitlab.com/alexis.bs/waddi-test/-/commit/83a70cb126d5d92f023a8201b8fb79a0bf9aba5e))
* add service , controller and module for post ([762f7b3](https://gitlab.com/alexis.bs/waddi-test/-/commit/762f7b36c2610c6f6c9f55bc740f9d9f49d017f3))
* add service controller and module for reviews ([049bf4f](https://gitlab.com/alexis.bs/waddi-test/-/commit/049bf4f3f48914694f36a30a67d29f5dff02e644))
* add service, controller of module for user ([67f33da](https://gitlab.com/alexis.bs/waddi-test/-/commit/67f33dafe056f2464b03de7f35936272fd87e4f8))
* add strategy for autentication ([1b7fa02](https://gitlab.com/alexis.bs/waddi-test/-/commit/1b7fa0221509df5bcf2e05df09b7a6ea1a58cc5e))
* provider , schema for reviews module ([59a9f08](https://gitlab.com/alexis.bs/waddi-test/-/commit/59a9f08ea8bf6f488617d36e3c8502da428cf6f0))


### Chore

* add last ignore files for eslintignore ([f07f7fb](https://gitlab.com/alexis.bs/waddi-test/-/commit/f07f7fb27b8c8ad6d7f4d5c5b92572c2f4170329))

## 0.2.0 (2023-02-19)


### Features

* add files of package.json ([92e2252](https://gitlab.com/alexis.bs/waddi-test/-/commit/92e22525c68b2e643cfc9f0d958bcf77dcd0dbbd))
* add husky library and conventional commit ([8fdf353](https://gitlab.com/alexis.bs/waddi-test/-/commit/8fdf353cbdaf1c33fcaba6945927140694d421ce))
* add prettier config && eslint config ([ef9ff29](https://gitlab.com/alexis.bs/waddi-test/-/commit/ef9ff29e192f259c263e5c6ad142eb4f8b111fde))
* clean space for work ([00dd8e9](https://gitlab.com/alexis.bs/waddi-test/-/commit/00dd8e9dbd55bdd6a8c3059319ac57d6ce2e4a10))
